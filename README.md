### Overview

It's a catalog of **i20** templates for [Generamba](https://github.com/rambler-digital-solutions/Generamba) code generator.

**Generamba** is a code generator made for working with Xcode. Primarily it is designed to generate VIPER modules but it is quite easy to customize it for generation of any other classes (both in Objective-C and Swift).

The detailed information about a template structure is available in [Generamba Wiki](https://github.com/rambler-digital-solutions/Generamba/wiki/Template-Structure).

### Templates
Parameters used with key  --custom-parameters. Example: ```generamba gen Calendar scotus_module --custom_parameters model:SBEvent```
* [scotus_module](https://gitlab.com/ximximik/generamba-templates/blob/master/scotus_module/scotus_module.rambaspec)
  * model (used for request dummy in Accessor, Parser and Preparer)

* [fit_mvvm_module](https://gitlab.com/ximximik/generamba-templates/blob/master/fit_mvvm_module/fit_mvvm_module.rambaspec)
* vc_name (name of view controller)
* model (used for request dummy in Accessor, Parser and Preparer. And also in VC, VM, CellVM)
* is_many (if true, in Accessor and VM will be generated array of values instead single object)
* is_table (if true, additionaly generate tableView in VC and connect it. Also, if is_many then additionaly generate methods in VC and VM to bind values to cells)
* cell (name of cell type, used only if is_table)
* is_dptable (if true, will be used DPTableView pod for generate methods in VC and replace class in Storyboard)
